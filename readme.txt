easypng is a simple, easy-to-use PNG image manipulation library created at the
University of Illinois for CS 225.

You will need libpng installed in order to use easypng.
